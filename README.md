# Sample for NUCLEO-F042K6

## Files
- F042K6_Sample.ioc: STM32CubeMX Project
- Src\main.c: Application logic

## Functions
- Blink onboard green LED (PB3) at 1Hz frequency (500ms off, 500ms on)
- Square wave output at 1Hz on PA12 / Pin D2
- Output current tick in millisecond on UART1 
    - USART1_TX: PA9 / Pin D1
    - USART1_RX: PA10 / Pin D0
- Output current tick in millisecond on UART2 (VCP on ST-Link) 
    - VCP_TX: PA2
    - VCP_RX: PA15 

## Pins

![](f042k6_pin.png)